#!/usr/bin/env python3

import os
import sys
from datetime import timedelta
from re import sub

from ics import Calendar, DisplayAlarm
from ics.grammar.parse import ContentLine
from requests import get as requests_get
from yaml import safe_load

seconds_per_unit = {"s": 1, "m": 60, "h": 3600, "d": 86400, "w": 604800}


def convert_to_seconds(s):
    return int(s[:-1]) * seconds_per_unit[s[-1]]


if __name__ == "__main__":
    script_dir = os.path.dirname(os.path.realpath(__file__))
    try:
        with open(os.path.join(script_dir, "config.yml")) as config_file:
            config = safe_load(config_file)
    except FileNotFoundError:
        print("config.yml not found")
        sys.exit(1)
    try:
        url = config.get("url", os.environ.get("URL"))
        if url:
            res = requests_get(url)
            res.raise_for_status()
            ics = sub(
                r"(CREATED|LAST-MODIFIED|DTSTAMP):\d{8}\/\d{2}\/\d{2}T\d{6}Z\n",
                "",
                res.text,
            )
            stadtreinigung = Calendar(ics)
        else:
            print("no calendar url set")
            sys.exit(1)
    except Exception:
        print("error fetching calendar from url")
        sys.exit(1)
    timezone = config.get("timezone", "Europe/Berlin")
    for event in stadtreinigung.events:
        begin = event.begin.to(timezone).replace(hour=config.get("begin"))
        event.begin = begin.to("utc")
        event.duration = timedelta(hours=1)
        # anonymization of uid and location which contains address
        if config.get("anonymization", True):
            event.location = None
            event.uid = "-".join(event.uid.split("-")[:3])
        event.alarms = []
        for reminder in config.get("reminders", []):
            seconds = convert_to_seconds(reminder)
            event.alarms.append(DisplayAlarm(trigger=timedelta(seconds=(seconds * -1))))
    # anonymization of X-WR-CALNAME and X-WR-CALDESC which contains address
    if config.get("anonymization", True):
        stadtreinigung.extra.clear()
        stadtreinigung.extra.append(
            ContentLine(name="X-WR-TIMEZONE", params={}, value=timezone)
        )
        stadtreinigung.extra.append(
            ContentLine(name="X-WR-CALNAME", params={}, value="Abfallkalender")
        )
        stadtreinigung.extra.append(
            ContentLine(name="X-WR-CALDESC", params={}, value="Abfallkalender")
        )
    with open(f"{script_dir}/abfallkalender.ics", "w") as f:
        f.write(str(stadtreinigung))
