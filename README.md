# Abfallkalender

Ein digitaler Kalender ohne Erinnerung nützt mir nicht viel, daher wird hier das ICS der [Stadtreinigung Leipzig](https://www.stadtreinigung-leipzig.de/leistungen/abfallentsorgung/abfallkalender-entsorgungstermine.html) eingelesen, die Termine auf einen konfigurierbaren (siehe `config.yml`) Startzeitpunkt inklusive Alarm gesetzt und als ICS zum abonnieren in den [Gitlab Pages](https://morph027.gitlab.io/abfallkalender/abfallkalender.ics) abgelegt.

Wer seine eigene Adresse parsen mag, einfach forken und die URL plus gegebenenfalls Startzeitpunkt/Erinnerung(en) anpassen und dann das ICS seines Forks abonnieren (Schema: `https://<namespace>.gitlab.io/abfallkalender/abfallkalender.ics`). 

## Konfiguration

* `url`: Die Kalenderurl der Stadtreinigung. Kann aus Datenschutzgründen auch als Umgebungsvariable `URL` gesetzt werden (in den CI/CD Einstellungen)
* `timezone`: Zielzeitzone für die Erinnerungen
* `begin`: Stunde des Terminbegins
* `reminders`: Liste mit Erinnerungen vor Terminbegin
